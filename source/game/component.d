module game.component;

import core.entity : Component;
import core.utils : Rect2i, Vec2d;

struct PositionComponent {
    Vec2d pos;
    alias pos this;
}

struct MotionComponent {
    Vec2d vel, acc;
}

struct AABBComponent {
    Rect2i rect;
    Margin margin;
    private struct Margin {
        int left, right, top, bottom;
    }

    @property int leftX() const { return rect.leftX() + margin.left; }
    @property int rightX() const { return rect.rightX() - margin.right; }
    @property int topY() const { return rect.topY() + margin.top; }
    @property int bottomY() const { return rect.bottomY() - margin.bottom; }
    @property Rect2i box() const { return Rect2i(leftX, topY, rightX-leftX+1, bottomY-topY+1); }

    this(Rect2i rect, Rect2i margin) {
        this.rect = rect;
        this.margin.left = margin.x; this.margin.right = margin.y;
        this.margin.top = margin.w; this.margin.bottom = margin.h;
    }
}

struct SpriteComponent {
    uint currentFrame = 0;
    uint maxFrame = 0;
    Rect2i[] frames;
    bool debugDraw = true;

    void addFrame(int x, int y, int w, int h) {
        frames ~= Rect2i(x, y, w, h);
        maxFrame += 1;
    }
}

struct HealthComponent {
    uint health = 1;
}
