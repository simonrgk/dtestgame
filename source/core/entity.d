module core.entity;

public import core.utils;


import std.stdio : writeln;
import std.conv : to;

public struct EntityHandle
{
    size_t id;
    uint tag;

    string toString() const {
        return "EntityHandle(" ~ to!string(this.id) ~ ", " ~ to!string(this.tag) ~ ")";
    }
}


public struct EntityInfo
{
    private {
        bool exists;
        bool wasCreated;
        bool[] activeComponents;
        bool[] updatedComponents;
    }
    /*
    @property bool exists() const { return m_exists; }
    @property bool exists(bool value) { return m_exists = value; }
    @property bool wasCreated() const { return m_wasCreated; }
    @property bool wasCreated(bool value) { return m_wasCreated = value; }
    */
    /// Initialize info about components. Used by EntityManager. 
    private void initializeComponents(size_t numComponents) {
        wasCreated = true;
        activeComponents = new bool[numComponents];
        updatedComponents = new bool[numComponents];
    }

    /// Check if the entity uses an component.
    bool hasComponent(C)() {
        assert(activeComponents != null);
        return activeComponents[ComponentId!(C)];
    }

    /// Check if the entity uses specified components.
    bool hasComponents(Cs...)() {
        static foreach(C; Cs) {
            if(!hasComponent!C) return false;
        }
        return true;
    }

    /// Activate an component. Used by EntityManager.
    private void activateComponent(C)() {
        assert(activeComponents != null);
        activeComponents[ComponentId!(C)] = true;
        updateComponent!C;
    }

    /// Deactivate an component. Used by EntityManager.
    private void deactivateComponent(C)() {
        assert(activeComponents != null);
        activeComponents[ComponentId!(C)] = false;
        updateComponent!C;        
    }

    /// Mark an component as updated. Used by EntityManager.
    private void updateComponent(C)() {
        assert(activeComponents != null);
        updatedComponents[ComponentId!(C)] = true;
    }

    string toString() const {
        return "EntityInfo(" ~ to!string(this.exists) ~ ", " ~ to!string(this.wasCreated) ~ ", " ~ to!string(this.activeComponents) ~ ", " ~ to!string(this.updatedComponents) ~ ")";
    }
}


private struct Entity
{
    EntityHandle handle;
    EntityInfo info;

    string toString() const {
        return "Entity(" ~ to!string(this.handle) ~ ", " ~ to!string(this.info) ~ ")";
    }
}



import core.utils : StaticCounter;

private class Component
{
    override string toString() const {
        return "Component(" ~ to!string(this.classinfo.name) ~ ")";
    }
}
alias ComponentId(C) = StaticCounter!(Component, C);



import std.variant : Variant;

private class ComponentPool
{
    private {
        size_t m_numEntities;
        Variant m_components;
        size_t m_id;
        void function(ComponentPool, ComponentPool, size_t) m_copyFunc;
    }

    @property size_t id() const { return m_id; }

    this(size_t numEntities) {
        m_numEntities = numEntities;        
        m_components = null;
    }

    void initialize(C)() {
        m_components = new C[m_numEntities];
        for(size_t i = 0; i < m_numEntities; i++)
            m_components[i] = C(); /* FDFDS */
        m_id = ComponentId!C;
        static void copyFunc(ComponentPool thisPool, ComponentPool otherPool, size_t id) {
            assert(thisPool.id == otherPool.id);
            thisPool.m_components.get!(C[])[id] = otherPool.m_components.get!(C[])[id];
        }
        m_copyFunc = &copyFunc;
    }

    /// Get a copy of a component of an entity with specific ID.
    C getComponent(C)(size_t id) {
        assert(m_components != null);
        assert(id < m_numEntities);
        assert(ComponentId!C == m_id);
        return (m_components.get!(C[])[id]);
    }

    /// Set a component of an entity with specific ID.
    void setComponent(C)(size_t id, C component) {
        assert(m_components != null);
        assert(id < m_numEntities);
        assert(ComponentId!C == m_id);
        m_components.get!(C[])[id] = component;
    }
    
    void copyComponent(ComponentPool otherPool, size_t id) {
        m_copyFunc(this, otherPool, id);
    }

    override string toString() const {
        assert(m_components != null);
        return "ComponentPool(" ~ to!string(this.classinfo.name) ~ ", " ~ to!string(this.m_components) ~ ")";
    }
}



private class EntityPool
{
    private {
        size_t m_numEntities;
        Entity[] m_entities;
        size_t m_numComponents;
        size_t m_freeComponentId;
        ComponentPool[] m_componentPool;
    }

    @property size_t numEntities() const { return m_numEntities; }

    invariant {
        assert(m_freeComponentId <= m_numComponents);
    }

    // Constructor.
    this(size_t numEntities, size_t numComponents) {
        m_numEntities = numEntities;
        m_entities = new Entity[m_numEntities];

        m_numComponents = numComponents;
        m_freeComponentId = 0;
        m_componentPool = new ComponentPool[m_numComponents];

        uint currentId = 0;
        for(size_t i = 0; i < m_numEntities; i++) {
            uint entityId = currentId++;
            m_entities[i].handle.id = entityId;
            m_entities[i].info.initializeComponents(m_numComponents);
        }
    }

    // Returns reference to an entity by index (for iteration).
    ref Entity opIndex(size_t i) {
        assert(i < m_numEntities);
        return m_entities[i];
    }

    /// Returns handle to new entity.
    EntityHandle createEntity() {
        for(size_t i = 0; i < m_numEntities; i++) {
            if(m_entities[i].info.exists) continue;
            m_entities[i].info.exists = true;
            m_entities[i].info.initializeComponents(m_numComponents);
            m_entities[i].handle.tag++;
            return m_entities[i].handle;
        }
        assert(false, "Failed to create new entity due to size limit.");
    }

    // Renders entity invalid in next frame.
    void invalidateEntity(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        m_entities[handle.id].info.exists = false;
        m_entities[handle.id].handle.tag++;
    }

    // Checks if the handle to an entity is still valid in current frame.
    bool isValidHandle(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        return m_entities[handle.id].info.exists && handle.tag == m_entities[handle.id].handle.tag;
    }

    /// Get the info about an entity in current frame.
    EntityInfo getEntityInfo(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        return m_entities[handle.id].info;
    }

    /// Register component.
    void registerComponent(C)() {
        assert(m_freeComponentId != m_numComponents);
        assert(!(StaticCounter!(Component, C) < m_freeComponentId));
        m_freeComponentId++;
        assert(StaticCounter!(Component, C) == m_freeComponentId-1);
        auto componentPool = new ComponentPool(m_numEntities);
        componentPool.initialize!C();
        m_componentPool[StaticCounter!(Component, C)] = componentPool;
        assert(StaticCounter!(Component, C) == m_componentPool[StaticCounter!(Component, C)].id);
    }

    // Register multiple components.
    void registerComponents(Cs...)() {
        static foreach(C; Cs) {
            registerComponent!C;
        }
    }
    
    /// Add a component to an entity.
    void activateComponent(C)(EntityHandle handle) {
        assert(isValidHandle(handle));
        m_entities[handle.id].info.activateComponent!C;
        assert(m_entities[handle.id].info.hasComponent!C);
    }

    /// Add multiple components to an entity.
    void activateComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            activateComponent!C(handle);
        }
    }

    /// Remove a component from an entity.
    void deactivateComponent(C)(EntityHandle handle) {
        assert(isValidHandle(handle));
        m_entities[handle.id].info.deactivateComponent!C;
        assert(!m_entities[handle.id].info.hasComponent!C);
    }
    
    /// Check if an entity has a component.
    bool hasComponent(C)(EntityHandle handle) {
        return getEntityInfo(handle).hasComponent!C;
    }

    /// Check if an entity has multiple component.
    bool hasComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            if(!hasComponent!C(handle)) return false;
        }
        return true;
    }

    /// Get a copy of the component.
    C getComponent(C)(EntityHandle handle) {
        assert(getEntityInfo(handle).hasComponent!C);
        return m_componentPool[StaticCounter!(Component, C)].getComponent!C(handle.id);
    }

    /// Set the value of the component.
    void setComponent(C)(EntityHandle handle, C component) {
        assert(getEntityInfo(handle).hasComponent!C);
        m_componentPool[StaticCounter!(Component, C)].setComponent!C(handle.id, component);
    }

    /// Mark the component of an entity as updated.
    void updateComponent(C)(EntityHandle handle) { //TODO: Enforce only one update per frame for correctness.
        assert(getEntityInfo(handle).hasComponent!C);
        m_entities[handle.id].info.updateComponent!C();
    }
}



public class EntityManager
{
    private {
        EntityPool[2] m_entityPools;
        size_t m_currentFrame;
    }

    @property size_t numEntities() const { return m_entityPools[currentFrame].m_numEntities; }
    @property size_t currentFrame() const { return m_currentFrame; }
    @property size_t nextFrame() const { return (m_currentFrame+1)%2; }


    // Constructor.
    this(size_t numEntities, size_t numComponents) {
        m_entityPools = [new EntityPool(numEntities, numComponents), new EntityPool(numEntities, numComponents)];
        m_currentFrame = 0;
    }

    // Returns handle to an entity from current frame by index (for iteration).
    ref EntityHandle opIndex(size_t i) {
        return m_entityPools[currentFrame][i].handle;
    }

    /// Returns handle to new entity that will be created in next frame.
    EntityHandle createEntity() {
        return m_entityPools[nextFrame].createEntity();
    }

    // Renders entity invalid in next frame.
    void invalidateEntity(EntityHandle handle) {
        m_entityPools[nextFrame].invalidateEntity(handle);
    }

    // Checks if the handle to an entity is still valid in current frame.
    bool isValidHandle(EntityHandle handle) {
        return m_entityPools[currentFrame].isValidHandle(handle); 
    }

    /// Get the info about an entity in current frame.
    EntityInfo getEntityInfo(EntityHandle handle) {
        return m_entityPools[currentFrame].getEntityInfo(handle);
    }

    /// Register component.
    void registerComponent(C)() {
        m_entityPools[currentFrame].registerComponent!C;
        m_entityPools[nextFrame].registerComponent!C;
    }

    // Register multiple components.
    void registerComponents(Cs...)() {
        static foreach(C; Cs) {
            registerComponent!C;
        }
    }

    /// Add a component to an entity in next frame.
    void activateComponent(C)(EntityHandle handle) {
        m_entityPools[nextFrame].activateComponent!C(handle);
    }

    /// Add multiple components to an entity in next frame.
    void activateComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            activateComponent!C(handle);
        }
    }

    /// Remove a component to an entity in next frame.
    void deactivateComponent(C)(EntityHandle handle) {
        m_entityPools[nextFrame].deactivateComponent!C(handle);
    }
    
    /// Check if an entity has a component in current frame.
    bool hasComponent(C)(EntityHandle handle) {
        return  m_entityPools[currentFrame].hasComponent!C(handle);
    }

    /// Check if an entity has multiple component in current frame.
    bool hasComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            if(!hasComponent!C(handle)) return false;
        }
        return true;
    }

    /// Get a copy of the component in current frame.
    C getComponent(C)(EntityHandle handle) {
        return m_entityPools[currentFrame].getComponent!C(handle);
    }

    /// Get a copy of the component from next frame.
    /// Used by systems updating components that dependent on transformed values of other components. 
    C getComponentFromNextFrame(C)(EntityHandle handle) {
        return m_entityPools[nextFrame].getComponent!C(handle);
    }

    /// Set value of the component in next frame and mark as updated.
    void setComponent(C)(EntityHandle handle, C component) {
        m_entityPools[nextFrame].setComponent!C(handle, component);
        m_entityPools[nextFrame].updateComponent!C(handle);
    }

    /// Treat next versions of the components as the current ones.
    void flipFrames() {
        m_currentFrame = (m_currentFrame+1)%2;

        for(size_t i = 0; i < numEntities; i++) {
            Entity entity = m_entityPools[currentFrame][i];
            writeln(entity.handle, entity.info);
            for(size_t k = 0; k < entity.info.updatedComponents.length; k++) {
                if(entity.info.updatedComponents[k] == true) {
                    m_entityPools[nextFrame][i].info.activeComponents[k] = m_entityPools[currentFrame][i].info.activeComponents[k];
                    m_entityPools[currentFrame][i].info.updatedComponents[k] = false;
                    m_entityPools[nextFrame].m_componentPool[k].copyComponent(m_entityPools[currentFrame].m_componentPool[k], entity.handle.id);
                }
            }
            if(m_entityPools[currentFrame][i].info.wasCreated) {
                m_entityPools[nextFrame][i].handle = entity.handle;
                m_entityPools[currentFrame][i].info.wasCreated = false;
                m_entityPools[nextFrame][i].info.exists = entity.info.exists;
                m_entityPools[nextFrame][i].info.wasCreated = false;
            }
            
        }
        /*writeln("------");
        for(size_t i = 0; i < numEntities; i++) {
            Entity entity = m_entityPools[nextFrame][i];
            writeln(entity.handle, entity.info);
             entity = m_entityPools[currentFrame][i];
            writeln(entity.handle, entity.info);
        }*/
    }
}



public class System
{
    void initialize() {}

    void update(EntityManager em, double dt) {}
}

public class SystemManager
{
    private {
        EntityManager m_entityManager;
        size_t m_numSystems;
        System[] m_systems;
    }

    this(EntityManager entityManager, size_t numSystems) {
        m_entityManager = entityManager;
        m_numSystems = numSystems;
        m_systems = new System[m_numSystems];
    }

    /// Add a system to the world.
    void registerSystem(S)()
    if (is (S : System)) {
        assert(StaticCounter!(System, S) < m_numSystems);
        m_systems[StaticCounter!(System, S)] = new S();
    }


    /// Get a system.
    inout(S) acquireSystem(S)() {
        if (T.classinfo.name in m_systems) {
            return cast(inout(T))m_systems[T.classinfo.name];
        } else {
            return null;
        }
    }

    /// Update systems using delta time.
    void update(double dt) {
        //import std.stdio : writeln;
        //writeln("update");

        /*foreach(ref entity; m_entities) {
            writeln(entity.id, " ", entity.tag, " ", entity.exists);
        }*/
        //assert(!m_entityManager.m_dirtyInfo);

        foreach(name, ref system; m_systems) {
            assert(system !is null);
            system.update(m_entityManager, dt);
        }
    }
}