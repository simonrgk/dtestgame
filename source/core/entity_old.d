module core.entity_old;

public import core.utils;


import std.stdio : writeln;
import std.conv : to;

public struct EntityHandle
{
    size_t id;
    uint tag;

    string toString() const {
        return "EntityHandle(" ~ to!string(this.id) ~ ", " ~ to!string(this.tag) ~ ")";
    }
}


public struct EntityInfo
{
    bool exists;
    bool wasUpdated;
    private bool[] activeComponents;

    /// Initialize info about components. Used by EntityManager. 
    private void initializeComponents(size_t numComponents) {
        activeComponents = new bool[numComponents];
    }

    /// Check if the entity uses an component.
    bool hasComponent(C)() {
        assert(activeComponents != null);
        return activeComponents[ComponentId!(C)];
    }

    /// Check if the entity uses specified components.
    bool hasComponents(Cs...)() {
        static foreach(C; Cs) {
            if(!hasComponent!C) return false;
        }
        return true;
    }

    /// Activate an component. Used by EntityManager.
    private void activateComponent(C)() {
        assert(activeComponents != null);
        activeComponents[ComponentId!(C)] = true;
    }

    /// Deactivate an component. Used by EntityManager.
    private void deactivateComponent(C)() {
        assert(activeComponents != null);
        activeComponents[ComponentId!(C)] = false;
    }

    string toString() const {
        return "EntityInfo(" ~ to!string(this.exists) ~ ", " ~ to!string(this.activeComponents) ~ ")";
    }
}


private struct Entity
{
    EntityHandle handle;
    EntityInfo info;

    string toString() const {
        return "Entity(" ~ to!string(this.handle) ~ ", " ~ to!string(this.info) ~ ")";
    }
}



import core.utils : StaticCounter;

private class Component
{
    override string toString() const {
        return "Component(" ~ to!string(this.classinfo.name) ~ ")";
    }
}
alias ComponentId(C) = StaticCounter!(Component, C);



import std.variant : Variant;

private class ComponentPool
{
    private {
        size_t m_numEntities;
        Variant m_components;
        size_t m_id;
    }

    @property size_t id() const { return m_id; }

    this(size_t numEntities) {
        m_numEntities = numEntities;        
        m_components = null;
    }

    void initialize(C)() {
        m_components = new C[m_numEntities];
        for(size_t i = 0; i < m_numEntities; i++)
            m_components[i] = C(); /* FDFDS */
        m_id = ComponentId!C;
    }

    /// Get a copy of a component of an entity with specific ID.
    C getComponent(C)(size_t id) {
        assert(m_components != null);
        assert(id < m_numEntities);
        assert(ComponentId!C == m_id);
        return (m_components.get!(C[])[id]);
    }

    /// Set a component of an entity with specific ID.
    void setComponent(C)(size_t id, C component) {
        assert(m_components != null);
        assert(id < m_numEntities);
        assert(ComponentId!C == m_id);
        m_components.get!(C[])[id] = component;
    }
    
    override string toString() const {
        assert(m_components != null);
        return "ComponentPool(" ~ to!string(this.classinfo.name) ~ ", " ~ to!string(this.m_components) ~ ")";
    }
}


///



public class EntityManager
{
    private {
        size_t m_numEntities;
        Entity[][2] m_entities;
        bool m_dirtyInfo;
        size_t m_numComponents;
        size_t m_freeComponentId;
        ComponentPool[][2] m_componentPools;
        size_t m_currentFrame;
    }

    @property size_t numEntities() const { return m_numEntities; }
    @property size_t currentFrame() const { return m_currentFrame; }
    @property size_t nextFrame() const { return (m_currentFrame+1)%2; }

    invariant {
        assert(m_freeComponentId <= m_numComponents);
    }

    // Constructor.
    this(size_t numEntities, size_t numComponents) {
        m_numEntities = numEntities;
        m_entities = [new Entity[m_numEntities], new Entity[m_numEntities]];
        m_dirtyInfo = false;

        m_numComponents = numComponents;
        m_freeComponentId = 0;
        m_componentPools = [new ComponentPool[m_numComponents], new ComponentPool[m_numComponents]];
        m_currentFrame = 0;

        uint currentId = 0;
        for(size_t i = 0; i < m_numEntities; i++) {
            uint entityId = currentId++;
            for(size_t k = 0; k < 2; k++) {
                m_entities[k][i].handle.id = entityId;
                m_entities[k][i].info.initializeComponents(m_numComponents);
            }
        }
    }

    // Returns entity handle by index (for iteration).
    ref EntityHandle opIndex(size_t i) {
        assert(i < m_numEntities);
        return m_entities[currentFrame][i].handle;
    }

    /// Returns handle to new entity.
    EntityHandle createEntity() {
        import std.algorithm : filter;
        /*auto freeEntities = m_entities.filter!(e => !e.info.exists); // TODO: Faster?
        if (freeEntities.empty)
            throw new Exception("Failed to create new entity due to size limit.");
        freeEntities.front.handle.tag++;
        freeEntities.front.info.exists = true;
        return freeEntities.front.handle;*/
        for(size_t i = 0; i < m_numEntities; i++) {
            if(m_entities[currentFrame][i].info.exists) continue;
            for(size_t k = 0; k < 2; k++) {
                m_entities[k][i].info.exists = true;
                m_entities[k][i].info.initializeComponents(m_numComponents);
                m_entities[k][i].handle.tag++;
            }
            return m_entities[currentFrame][i].handle;
        }
        throw new Exception("Failed to create new entity due to size limit.");
    }

    // Renders entity invalid in next frame.
    void invalidateEntity(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        m_entities[nextFrame][handle.id].info.exists = false;
        m_entities[nextFrame][handle.id].handle.tag++;
    }

    // Checks if the handle to an entity is still valid in current frame.
    bool isValidHandle(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        return m_entities[currentFrame][handle.id].info.exists && handle.tag == m_entities[currentFrame][handle.id].handle.tag;
    }

    /// Get the info about an entity in current frame.
    EntityInfo getEntityInfo(EntityHandle handle) {
        assert(handle.id < m_numEntities);
        return m_entities[currentFrame][handle.id].info;
    }

    /// Register component.
    void registerComponent(C)() {
        assert(!(StaticCounter!(Component, C) < m_freeComponentId));
        m_freeComponentId++;
        assert(StaticCounter!(Component, C) == m_freeComponentId-1);

        for(size_t i = 0; i < 2; i++) {
            auto componentPool = new ComponentPool(m_numEntities);
            componentPool.initialize!C();
            m_componentPools[i][StaticCounter!(Component, C)] = componentPool;
            assert(StaticCounter!(Component, C) == m_componentPools[i][StaticCounter!(Component, C)].id);
        }
    }

    // Register multiple components.
    void registerComponents(Cs...)() {
        static foreach(C; Cs) {
            registerComponent!C;
        }
    }
    
    /// Add a component to an entity in both frames.
    void initializeComponent(C)(EntityHandle handle) {
        assert(isValidHandle(handle));
        for(size_t k = 0; k < 2; k++) {
            m_entities[k][handle.id].info.activateComponent!C;
            assert(m_entities[k][handle.id].info.hasComponent!C);
        }
    }

    /// Add multiple components to an entity in both frames.
    void initializeComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            initializeComponent!C(handle);
        }
    }

    /// Add a component to an entity in current frame.
    void activateComponent(C)(EntityHandle handle) {
        assert(isValidHandle(handle));
        m_entities[currentFrame][handle.id].info.activateComponent!C;
        m_dirtyInfo = true;
        assert(m_entities[currentFrame][handle.id].info.hasComponent!C);
    }

    /// Add multiple components to an entity in next frame.
    void activateComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            activateComponent!C(handle);
        }
    }

    /// Remove a component to an entity in next frame.
    void deactivateComponent(C)(EntityHandle handle) {
        assert(isValidHandle(handle));
        m_entities[nextFrame][handle.id].info.deactivateComponent!C;
        m_dirtyInfo = true;
        assert(!m_entities[handle.id].info.hasComponent!C);
    }
    
    /// Check if an entity has a component in current frame.
    bool hasComponent(C)(EntityHandle handle) {
        return getEntityInfo(handle).hasComponent!C;
    }

    /// Check if an entity has multiple component in current frame.
    bool hasComponents(Cs...)(EntityHandle handle) {
        static foreach(C; Cs) {
            if(!hasComponent!C(handle)) return false;
        }
        return true;
    }

    /// Get a copy of the current version of the component.
    C getComponent(C)(EntityHandle handle) {
        assert(getEntityInfo(handle).hasComponent!C);
        return m_componentPools[currentFrame][StaticCounter!(Component, C)].getComponent!C(handle.id);
    }

    /// Set value of the component.
    void setComponent(C)(EntityHandle handle, C component) {
        assert(getEntityInfo(handle).hasComponent!C);
        m_componentPools[currentFrame][StaticCounter!(Component, C)].setComponent!C(handle.id, component);
        m_componentPools[nextFrame][StaticCounter!(Component, C)].setComponent!C(handle.id, component);
    }

    /// Set the next version of the component of an entity.
    void updateComponent(C)(EntityHandle handle, C component) { //TODO: Enforce only one update per frame for correctness.
        assert(getEntityInfo(handle).hasComponent!C);
        m_componentPools[nextFrame][StaticCounter!(Component, C)].setComponent!C(handle.id, component);
    }

    /// Treat next versions of the components as the current ones.
    void updateComponents() {
        m_currentFrame = (m_currentFrame+1)%2;
        if(m_dirtyInfo) {

        }
    }
}


public class System
{
    void initialize() {}

    void update(EntityManager em, double dt) {}
}

public class SystemManager
{
    private {
        EntityManager m_entityManager;
        size_t m_numSystems;
        System[] m_systems;
    }

    this(EntityManager entityManager, size_t numSystems) {
        m_entityManager = entityManager;
        m_numSystems = numSystems;
        m_systems = new System[m_numSystems];
    }

    /// Add a system to the world.
    void registerSystem(S)()
    if (is (S : System)) {
        assert(StaticCounter!(System, S) < m_numSystems);
        m_systems[StaticCounter!(System, S)] = new S();
    }


    /// Get a system.
    inout(S) acquireSystem(S)() {
        if (T.classinfo.name in m_systems) {
            return cast(inout(T))m_systems[T.classinfo.name];
        } else {
            return null;
        }
    }

    /// Update systems using delta time.
    void update(double dt) {
        //import std.stdio : writeln;
        //writeln("update");

        /*foreach(ref entity; m_entities) {
            writeln(entity.id, " ", entity.tag, " ", entity.exists);
        }*/
        //assert(!m_entityManager.m_dirtyInfo);

        entityLoop:
        foreach(name, ref system; m_systems) {
            assert(system !is null);
            system.update(m_entityManager, dt);
        }
    }
}