module core.renderer;

public import core.utils;

import derelict.sdl2.sdl,
	   derelict.sdl2.image,
	   derelict.sdl2.mixer,
	   derelict.sdl2.ttf,
	   derelict.sdl2.net;

import std.stdio : writeln;
import std.algorithm : min;
import std.conv : to;

class Renderer
{
    private {
        uint m_windowWidth;
        uint m_windowHeight;
        SDL_Window *m_sdlWindow;
        SDL_Renderer *m_sdlRenderer;
        SDL_RendererInfo m_sdlRendererInfo;
        SDL_PixelFormat *m_sdlPixelFormat;
        SDL_Surface *m_sdlImage;
        SDL_Rect m_sdlImageRect;
        SDL_Texture *m_sdlTexture;
        uint m_screenWidth;
        uint m_screenHeight;
        uint m_pixelScale;
        Vec2i m_screenPosition;
    }

    @property uint windowWidth() const { return m_windowWidth; }
    @property uint windowHeight() const { return m_windowHeight; }
    @property uint screenWidth() const { return m_screenWidth; }
    @property uint screenHeight() const { return m_screenHeight; }
    @property uint pixelScale() const { return m_pixelScale; }
    @property Vec2i screenPosition() const { return m_screenPosition; }

    invariant {
        assert(m_sdlWindow != null);
        assert(m_sdlRenderer != null);
        assert(m_sdlPixelFormat != null);
        assert(m_sdlImage != null);
        assert(m_sdlImageRect != SDL_Rect.init);
        assert(m_sdlTexture != null);
    }
    
    this(const char[] windowTitle, uint windowWidth, uint windowHeight, uint screenHeight, uint screenWidth) {
        this.m_windowWidth = windowWidth;
        this.m_windowHeight = windowHeight;
        this.m_screenWidth = screenWidth;
        this.m_screenHeight = screenHeight;
        writeln("window: ", m_windowWidth, "x", m_windowHeight);

        // load bindings
        DerelictSDL2.load(SharedLibVersion(2, 0, 0));
        DerelictSDL2Image.load();
        DerelictSDL2Mixer.load();
        DerelictSDL2ttf.load();
        DerelictSDL2Net.load();

        // initialize SDL
        if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
            throw new Exception("Failed to initialize SDL: " ~ to!string(SDL_GetError()));
        
        // create window
        m_sdlWindow = SDL_CreateWindow("SDL2 Example", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, SDL_WINDOW_SHOWN);
        if (!m_sdlWindow)
            throw new Exception("Failed to create a SDL window: " ~ to!string(SDL_GetError()));

        // create renderer
        m_sdlRenderer = SDL_CreateRenderer(m_sdlWindow, -1, SDL_RENDERER_ACCELERATED);
        if (!m_sdlRenderer)
            throw new Exception("Failed to create a SDL renderer: " ~ to!string(SDL_GetError()));

        // get render info
        if (SDL_GetRendererInfo(m_sdlRenderer, &m_sdlRendererInfo) != 0)
            throw new Exception("Failed to get a render info: " ~ to!string(SDL_GetError()));
        m_sdlPixelFormat = SDL_AllocFormat(m_sdlRendererInfo.texture_formats[0]); // TODO: Delete?
        import std.string : fromStringz;
        writeln("renderer: ", fromStringz(m_sdlRendererInfo.name));
        writeln("texure_format: ", fromStringz(SDL_GetPixelFormatName(m_sdlRendererInfo.texture_formats[0])));

        // load image sheet
        m_sdlImage = SDL_LoadBMP("./resources/sheet.bmp");
        if (!m_sdlImage)
            throw new Exception("Failed to load image sheet: " ~ to!string(SDL_GetError()));
        if (SDL_SetColorKey(m_sdlImage, SDL_TRUE, SDL_MapRGB(m_sdlImage.format, 255, 0, 255)) != 0)
            throw new Exception("Failed to set color key: " ~ to!string(SDL_GetError()));
        m_sdlImageRect.x = m_sdlImageRect.y = 0;
        m_sdlImageRect.w = m_sdlImage.w; m_sdlImageRect.h = m_sdlImage.h;
        writeln("sheet: ", m_sdlImageRect.w, "x", m_sdlImageRect.h);

        // create texture from image sheet
        m_sdlTexture = SDL_CreateTextureFromSurface(m_sdlRenderer, m_sdlImage);
        if (!m_sdlTexture)
            throw new Exception("Failed to create texture from image sheet: " ~ to!string(SDL_GetError()));

        // initialize screen
        this.recalculateScreenFields();
        writeln("screen: ", m_screenWidth, "x", m_screenHeight, " * ", m_pixelScale, " (", m_screenPosition.x, ", ", m_screenPosition.y, ")");

    }

    ~this() {
        SDL_DestroyTexture(m_sdlTexture);
        SDL_FreeSurface(m_sdlImage);
        SDL_DestroyRenderer(m_sdlRenderer);
        SDL_DestroyWindow(m_sdlWindow);
        SDL_Quit();
    }
    
    void renderSprite(int x, int y, Rect2i src, bool flipHorizontally, bool flipVertically) {
        SDL_Rect dst = SDL_Rect(m_screenPosition.x + x * m_pixelScale, m_screenPosition.y + y * m_pixelScale, src.w * m_pixelScale, src.h * m_pixelScale);
        SDL_RenderCopyEx(m_sdlRenderer, m_sdlTexture, &cast(SDL_Rect)src, &dst, 0.0, null, (flipHorizontally ? SDL_FLIP_HORIZONTAL : SDL_FLIP_NONE) | (flipVertically ? SDL_FLIP_VERTICAL : SDL_FLIP_NONE));
    }

    void renderRect(Rect2i rect, ubyte r = 255, ubyte g = 255, ubyte b = 255) {
        SDL_Rect sdlRect = SDL_Rect(m_screenPosition.x/m_pixelScale + rect.x, m_screenPosition.y/m_pixelScale + rect.y, rect.w, rect.h);
        SDL_SetRenderDrawColor(m_sdlRenderer, r, g, b, 255);
        SDL_RenderSetScale(m_sdlRenderer, m_pixelScale, m_pixelScale);
        SDL_RenderDrawRect(m_sdlRenderer, &cast(SDL_Rect)sdlRect);
        SDL_RenderSetScale(m_sdlRenderer, 1, 1);
    }


    void startMainLoop(void delegate(double dt) update, void delegate() draw) {
        const uint fps = 1;
        const double dt = 1.0 / cast(double)fps;
        uint frame_count = 0;
        uint elapsed_time = 0;
        uint frame_last = SDL_GetTicks();
        double acc = 0.0;

        // SDL main loop
        SDL_Event sdl_event;
        bool done = false;
        while (!done) {
            // calculating FPS
            uint frame_start = SDL_GetTicks();
            uint frame_time = frame_start - frame_last;
            frame_last = frame_start;
            elapsed_time += frame_time;
            frame_count++;
            if (elapsed_time >= 1000) {
                writeln("FPS: ", frame_count);
                frame_count = 0;
                elapsed_time = 0;
            }
            acc += frame_time / 1000.0f; // in seconds
            //writeln("ft: ", frame_time);
            //writeln("update");
            while(acc > dt) {
                // process SDL events
                while (SDL_PollEvent(&sdl_event)) {
                    if (sdl_event.type == SDL_QUIT) {
                        done = true;
                        break;
                    }
                }

                writeln(dt);
                // update
                update(dt);
                acc -= dt;
            }
            
            // draw
            SDL_SetRenderDrawColor(m_sdlRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
            SDL_RenderClear(m_sdlRenderer);
            debug {
                SDL_SetRenderDrawColor(m_sdlRenderer, 100, 100, 100, SDL_ALPHA_OPAQUE);
                SDL_Rect screenRect = SDL_Rect(m_screenPosition.x, m_screenPosition.y, m_screenWidth * m_pixelScale, m_screenHeight * m_pixelScale);
                SDL_RenderDrawRect(m_sdlRenderer, &screenRect);
                SDL_SetRenderDrawColor(m_sdlRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
            }
            draw();
            SDL_RenderPresent(m_sdlRenderer);
        }
    }

    private void recalculateScreenFields() {
        m_pixelScale = min(windowWidth / screenWidth, windowHeight / screenHeight);
        m_screenPosition = Vec2i((windowWidth - screenWidth * pixelScale) / 2, (windowHeight - screenHeight * pixelScale) / 2);
    }
}
