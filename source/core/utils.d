module core.utils;

import std.conv : to;


// Math.
struct Vec2(T)
{
    T x = 0;
    T y = 0;
    
    this(T x, T y) {
        this.x = x;
        this.y = y;
    }

    string toString() const {
        return "(" ~ to!string(this.x) ~ ", " ~ to!string(this.y) ~ ")";
    }

}
alias Vec2i = Vec2!int;
alias Vec2f = Vec2!float;
alias Vec2d = Vec2!double;


import derelict.sdl2.sdl : SDL_Rect;

struct Rect2i
{
    this(int x, int y, int w, int h) {
        _rect = SDL_Rect(x, y, w, h);
    }
    
    @property int leftX() const { return x; }
    @property int rightX() const { return x + w - 1; } // x + w is outside of rect, hence -1
    @property int topY() const { return y; }
    @property int bottomY() const { return y + h - 1; } // y + h is outside of rect, hence -1

    alias _rect this;
    SDL_Rect _rect;
}


//Static counters.
struct StaticCounterData(T)
{
    static size_t counter = 0;
}

struct StaticCounterImpl(T, U)
{
    private StaticCounterData!T staticCounterData;

    public static size_t getCounter()
    {
        static size_t counter = -1;

        if(counter == -1)
        {
            counter = staticCounterData.counter;
            staticCounterData.counter++;
        }

        return counter;
    }
}
alias StaticCounter(T, U) = StaticCounterImpl!(T, U).getCounter;