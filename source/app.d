#!/usr/bin/env rdmd

import core.renderer : Renderer;

import core.entity : Entity, EntityHandle, EntityManager, System, SystemManager;
import std.stdio : writeln;

import game.component : PositionComponent, MotionComponent, AABBComponent, SpriteComponent, HealthComponent;
import core.utils : Vec2d, Rect2i;


class MovementSystem : System {
    override void initialize() {
    }

    override void update(EntityManager em, double dt) {
        for(size_t i = 0; i < em.numEntities; i++) {
            EntityHandle handle = em[i];

            if(!em.hasComponents!(PositionComponent, MotionComponent)(handle)) continue;
            
            PositionComponent position = em.getComponent!PositionComponent(handle);
            MotionComponent motion = em.getComponent!MotionComponent(handle);

            position.x += motion.vel.x * dt;
            position.y += motion.vel.y * dt;
            motion.vel.x += motion.acc.x * dt;
            motion.vel.y += motion.acc.y * dt;

            em.setComponent!PositionComponent(handle, position);
            em.setComponent!MotionComponent(handle, motion);
        }
    }
}

/// Update position of AABBComponent using PositionComponent transformed by previous systems.
class UpdateAABBSystem : System {
    override void initialize() {
    }

    override void update(EntityManager em, double dt) {
        for(size_t i = 0; i < em.numEntities; i++) {
            EntityHandle handle = em[i];

            if(!em.hasComponents!(PositionComponent, AABBComponent)(handle)) continue;
            
            auto pos = em.getComponentFromNextFrame!PositionComponent(handle);
            auto aabb = em.getComponent!AABBComponent(handle);
            writeln(pos);
            writeln(aabb);
            
            aabb.rect.x = cast(int)pos.x;
            aabb.rect.y = cast(int)pos.y;

            em.setComponent!AABBComponent(handle, aabb);
        }
    }
}

class HealthSystem : System {
    override void initialize() {
    }

    override void update(EntityManager em, double dt) {
        for(size_t i = 0; i < em.numEntities; i++) {
            EntityHandle handle = em[i];

            if(!em.hasComponents!(HealthComponent)(handle)) continue;
            
            HealthComponent health = em.getComponent!HealthComponent(handle);

            if(health.health > 0) {
                health.health -= 1;
                em.setComponent!HealthComponent(handle, health);
            } else {
                em.deactivateComponent!HealthComponent(handle);
                em.activateComponent!MotionComponent(handle);
                em.setComponent(handle, MotionComponent(Vec2d(20, -30), Vec2d(0, 30)));
            }
        }
    }
}

void main() {
    size_t numEntities = 4;
    size_t numComponents = 5;
    size_t numSystems = 3;

    EntityManager em = new EntityManager(numEntities, numComponents);

    em.registerComponents!(PositionComponent, MotionComponent, AABBComponent, SpriteComponent, HealthComponent);

    EntityHandle e1 = em.createEntity();
    em.activateComponents!(PositionComponent, MotionComponent, AABBComponent, SpriteComponent)(e1);
    em.setComponent(e1, PositionComponent(Vec2d(50, 60)));
    em.setComponent(e1, MotionComponent(Vec2d(0, -40), Vec2d(0, 20)));
    em.setComponent(e1, AABBComponent(Rect2i(0, 0, 16, 16), Rect2i(1, 1, 1, 0)));
    {
    SpriteComponent spr;
    spr.addFrame(30, 0, 16, 16);
    em.setComponent(e1, spr);
    }

    EntityHandle e2 = em.createEntity();
    em.activateComponents!(PositionComponent, AABBComponent, SpriteComponent)(e2);
    em.setComponent(e2, PositionComponent(Vec2d(50, 100)));
    em.setComponent(e2, AABBComponent(Rect2i(0, 0, 16, 16), Rect2i(1, 1, 1, 1)));
    {
    SpriteComponent spr;
    spr.addFrame(90, 0, 16, 16);
    em.setComponent(e2, spr);
    }

    SystemManager systems = new SystemManager(em, numSystems);
    systems.registerSystem!MovementSystem;
    systems.registerSystem!UpdateAABBSystem;
    systems.registerSystem!HealthSystem;


    Renderer renderer = new Renderer("Test", 640, 480, 200, 200);
    void update(double dt) {
        systems.update(dt);
        em.flipFrames();
    }
    void draw() { // TODO: Semaphore?
        for(size_t i = 0; i < em.numEntities; i++) {
            EntityHandle handle = em[i];

            if(!em.hasComponents!(PositionComponent, SpriteComponent)(handle)) continue;
            auto pos = em.getComponent!PositionComponent(handle);
            auto spr = em.getComponent!SpriteComponent(handle); 

            assert(spr.frames.length > 0);
            assert(spr.currentFrame < spr.frames.length);
            
            renderer.renderSprite(cast(int)pos.x, cast(int)pos.y, spr.frames[spr.currentFrame], false, false);
            if(spr.debugDraw) {
                if(em.hasComponent!AABBComponent(handle)) {
                    auto aabb = em.getComponent!AABBComponent(handle);
                    renderer.renderRect(aabb.rect, 255, 0, 0);
                    renderer.renderRect(aabb.box, 255, 255, 0);
                }
            }
        }
    }
    renderer.startMainLoop(&update, &draw);
}